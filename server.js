const http = require('http');
const url = require('url');


const htmlContent = `
<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
    <p> - Martin Fowler</p>

</body>
</html>
`

const jsonContent = `
{
    "slideshow": {
      "author": "Yours Truly",
      "date": "date of publication",
      "slides": [
        {
          "title": "Wake up to WonderWidgets!",
          "type": "all"
        },
        {
          "items": [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets"
          ],
          "title": "Overview",
          "type": "all"
        }
      ],
      "title": "Sample Slide Show"
    }
}
`

const uuidContent = `
{
    "uuid": "14d96bb1-5d53-472f-a96e-b3a1fa82addd"
}
`

// This is function to start the server and serve the requests.

http.createServer( function (request, response) {

    // Get the path name from url
    let pathName = url.parse(request.url, true).pathname;

    // if path name is /html
    if (pathName == '/html') {

        response.writeHead(200, {
            'Content-Type': 'text/html'
        });
        response.write(htmlContent);
        response.end();

    // Else if path name is /json
    } else if (pathName == '/json') {

        response.writeHead(200, {
            'Content-Type': 'JSON'
        });
        response.write(jsonContent);
        response.end();
    
    // Else if path name is uuid
    } else if (pathName == '/uuid') {

        response.writeHead(200, {
            'Content-type': 'JSON'
        });
        response.write(uuidContent);
        response.end();
    
    // Else if path name is /status/{statuscode}
    } else if ( pathName.startsWith('/status/') ) {
        
        let statusCode = parseInt(pathName.split('/')[2]);
        
        response.writeHead(statusCode);
        response.write('StatusCode' + statusCode);
        response.end();

    // IF path name is /delay/{time}
    } else if ( pathName.startsWith('/delay/') ) {

        let delay = parseInt(pathName.split('/')[2]);
        
        setTimeout(() => {

            response.writeHead(200);
            response.write(`Response after ${delay} sec`);
            response.end();

        }, delay * 1000)
        
    } else {
        response.writeHead(404)
        response.end();
    }

}).listen(8080);